package com.famous.coader.countrycodepicker

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import com.hbb20.CountryCodePicker

class MainActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //finding elements using findViewById
        val countryCodePicker = findViewById<CountryCodePicker>(R.id.countyCodePicker)
        val countryFlag = findViewById<ImageView>(R.id.flag_imageView)

        // set click event using setOnCountryChangeListener for countryName, countryCode, countryCodeName, countryFlagImage and more
        countryCodePicker.setOnCountryChangeListener {

            val countryName = countryCodePicker.selectedCountryName
            val countryCode = countryCodePicker.selectedCountryCode
            val countryCodeName = countryCodePicker.selectedCountryNameCode
            val countryCodeWithPlus = countryCodePicker.selectedCountryCodeWithPlus
            val countryFlagImage = countryCodePicker.selectedCountryFlagResourceId

            // set selected flag in image view
            countryFlag.setImageResource(countryFlagImage)

            Toast.makeText(this, "$countryName, $countryCode, $countryCodeName, $countryCodeWithPlus", Toast.LENGTH_SHORT).show()

        }
    }
}